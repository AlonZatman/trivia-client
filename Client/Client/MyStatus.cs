﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
namespace Client
{
    public partial class MyStatus : Form
    {
        private static string name;
        private static TcpClient tcpInt;
        public MyStatus(string _name,TcpClient t)
        {
            InitializeComponent();
            name = _name;
            tcpInt = t;
        }

        private void handleThread()
        {
            Byte[] data;
            NetworkStream stream = tcpInt.GetStream();

            while (1 == 1)
            {

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length); //(**This receives the data using the byte method**)
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

                if(responseData.Substring(0,3)=="126")
                {
                    if(responseData.Substring(3,4)!="0000")
                    {
                         Invoke((MethodInvoker)delegate 
                        {
                            lblNumOfGames.Text += int.Parse(responseData.Substring(3,4));
                            lblRight.Text += int.Parse(responseData.Substring(7, 6));
                            lblWrong.Text += int.Parse(responseData.Substring(13, 6));
                            float time = int.Parse(responseData.Substring(19, 4));
                            time /= 100;
                            lblTime.Text += time;
                        });
                    }
                    else
                    {
                        Invoke((MethodInvoker)delegate 
                        {
                            lblNumOfGames.Text += "0";
                            lblRight.Text += "0";
                            lblWrong.Text += "0";
                            lblTime.Text += "0";
                        });
                    }
                    break;
                }
           
            }

        }

        private void MyStatus_Load(object sender, EventArgs e)
        {
            askForStatus();
            handleThread();
        }

        private void askForStatus()
        {
            string msg = "225";

            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new Signed(name, tcpInt);
            wnd.ShowDialog();
            this.Close();     
        }
    }
}
