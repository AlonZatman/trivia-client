﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
namespace Client
{
    public partial class BestScores : Form
    {
        private static string name;
        private static TcpClient tcpInt;
        public BestScores(string _name, TcpClient t)
        {
            InitializeComponent();
            name = _name;
            tcpInt = t;
        }

        private void handleThread()
        {
            Byte[] data;
            NetworkStream stream = tcpInt.GetStream();
            int len = 0;
            int score = 0;
            while (1 == 1)
            {

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length); //(**This receives the data using the byte method**)
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                if (responseData.Substring(0, 3) == "124")
                {
                    responseData = responseData.Substring(3);
                    for (int i = 0; i < 3; i++)
                    {
                        len = int.Parse(responseData.Substring(0, 2));
                        if (len == 0)
                        {
                            responseData = responseData.Substring(len + 6 + 2);
                            continue;
                        }
                        else
                        {
                            score = int.Parse(responseData.Substring(len + 2, 6));
                            Invoke((MethodInvoker)delegate
                            {
                                lstbxPlayers.Items.Add(responseData.Substring(2, len) + " - " + score);
                            });
                        }
                        responseData = responseData.Substring(len + 6 + 2);

                    }
                    break;
                }
            }

        }

        private void BestScores_Load(object sender, EventArgs e)
        {
            askForBestScores();
            handleThread();
        }

        private void askForBestScores()
        {

            string msg = "223";

            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new Signed(name, tcpInt);
            wnd.ShowDialog();
            this.Close();

        }

    }
}
