﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class CreateRoom : Form
    {
        string name;
        TcpClient tcpInt;
        string roomName;
        string time;
        String questions;
        string maxPlayers;
        public CreateRoom(string userName, TcpClient t)
        {
            InitializeComponent();
            name = userName;
            tcpInt = t;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new Signed(name,tcpInt);
            wnd.ShowDialog();
            this.Close();     
        }

        private void handleThread(TcpClient client)
        {
            Byte[] data;
            NetworkStream stream = tcpInt.GetStream();

            while (1 == 1)
            {

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length); //(**This receives the data using the byte method**)
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

                switch (responseData)
                {
                    case "1141":
                        Invoke((MethodInvoker)delegate { lblError.Text = "Error creating room"; });
                        break;
                    case "1140":
                        Invoke((MethodInvoker)delegate
                        {
                            this.Hide();
                            Form wnd = new AdminRoom(name,maxPlayers,time,questions,roomName,tcpInt);
                            wnd.ShowDialog();
                            this.Close();
                        });
                        break;
                    default:
                        break;
                }
            }

        }

        private string padding(int num)
        {
            if (num > 9)
            {
                return num.ToString();
            }
            else
            {
                string ret = num.ToString();
                ret = ret.PadLeft(2, '0');
                return ret;
            }

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if(int.Parse(txtNumPlayers.Text) >9 )
            {
                lblError.Text = "Error! number of players invalid";
            }else
            {
                time = txtQuestionTime.Text;
                roomName = txtRoomName.Text;
                questions = txtQusetionNum.Text;
                maxPlayers = txtNumPlayers.Text;
                string send = "213";
                send += padding(txtRoomName.Text.Length) + txtRoomName.Text + txtNumPlayers.Text + padding(int.Parse(txtQusetionNum.Text)) + padding(int.Parse(txtQuestionTime.Text));

                Byte[] data = System.Text.Encoding.ASCII.GetBytes(send);
                // Get a client stream for reading and writing. 
                NetworkStream stream = tcpInt.GetStream();

                // Send the message to the connected TcpServer. 
                stream.Write(data, 0, data.Length); //(**This is to send data using the byte method**)  
            }
            
        }

        private void CreateRoom_Load(object sender, EventArgs e)
        {
            Thread t = new Thread(() => handleThread(tcpInt));
            t.Start();
        }
    }
}
