﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Client
{
   
    public partial class Form1 : Form
    {
        private static TcpClient tcpInt;
        public static string user;
        public static Form mainForm;
        
        public Form form
        {
            get
            {
                return mainForm;
            }
           
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void handleSignIn(string msg)
        {
            if(msg[3]=='0')
            {
                Invoke((MethodInvoker)delegate
                {
                    this.Hide();
                    Form wnd = new Signed(txtUser.Text,tcpInt);
                    wnd.ShowDialog();
                    this.Close();
                });
              
            }
        }
 
        private void handleThread(TcpClient client)
        {
            Byte[] data;
            NetworkStream stream = tcpInt.GetStream();
           
            while(1==1)
            {

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length); //(**This receives the data using the byte method**)
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

                switch (responseData)
                {
                    case "1020":
                        handleSignIn(responseData);
                        break;
                    case "1021":
                        Invoke((MethodInvoker)delegate { lblError.Text = "Error,wrong details"; lblError.Visible = true; });
                        break;
                    case "1022":
                        Invoke((MethodInvoker)delegate { lblError.Text = "User already signed in"; lblError.Visible = true; });
                        break;
                    default:
                        break;
                }
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            mainForm = this;
            try
            {
                tcpInt = new TcpClient();
                tcpInt.Connect("127.0.0.1", 8820);
                Thread t = new Thread(() => handleThread(tcpInt));
                t.Start();
            }
            catch(Exception e1)
            {
                MessageBox.Show(e1.ToString());
                Application.Exit();
            }

        }

        private void lblUser_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new SignUp(tcpInt);
            wnd.ShowDialog();
        }

        //private void btnJoinRoom_Click(object sender, EventArgs e)
        //{
        //   
        //    
        //}

        private void btnExit_Click(object sender, EventArgs e)
        {
            string send = "201";
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(send);

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length); 

            Application.Exit();
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            string username = txtUser.Text;
            string password = txtPass.Text;
            string send = "200" + padding(username.Length) + username + padding(password.Length) + password;
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(send);
            user = txtUser.Text;
            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            // Send the message to the connected TcpServer. 
            stream.Write(data, 0, data.Length); //(**This is to send data using the byte method**)       
        }

        private string padding(int num)
        {
            if(num>9)
            {
                return num.ToString();
            }
            else
            {
                string ret = num.ToString();
                ret = ret.PadLeft(2, '0');
                return ret;
            }
            
        }

        private void btnSignOut_Click(object sender, EventArgs e)
        {
            string send = "201";
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(send);
           
            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            // Send the message to the connected TcpServer. 
            stream.Write(data, 0, data.Length); //(**This is to send data using the byte method**) 
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        //private void btnCreateRoom_Click(object sender, EventArgs e)
        //{
        //   
        //}
       
    }
}
 