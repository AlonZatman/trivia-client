﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class SignUp : Form
    {
        TcpClient t;
        public SignUp(TcpClient tcpInt)
        {
            InitializeComponent();
            t = tcpInt;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new Form1();
            wnd.ShowDialog();
            this.Close();
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            string msg = "203";
            msg += padding(txtUserName.Text.Length) + txtUserName.Text + padding(txtPassword.Text.Length) + txtPassword.Text + padding(txtEmail.Text.Length) + txtEmail.Text;

            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

            // Get a client stream for reading and writing. 
            NetworkStream stream = t.GetStream();

            stream.Write(data, 0, data.Length); 

        }

        private void handleThread(TcpClient client)
        {
            Byte[] data;
            NetworkStream stream = t.GetStream();

            while (1 == 1)
            {

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length); //(**This receives the data using the byte method**)
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

                switch (responseData)
                {
                    case "1040":
                        Invoke((MethodInvoker)delegate
                        {
                            this.Hide();
                            Form wnd = new Form1();
                            wnd.ShowDialog();
                            this.Close();
                        });
                        break;
                    case "1041":
                        Invoke((MethodInvoker)delegate { lblError.Text = "Pass illegal"; });
                        break;
                    case "1042":
                        Invoke((MethodInvoker)delegate { lblError.Text = "Username is already exists"; });
                        break;
                    case "1043":
                        Invoke((MethodInvoker)delegate { lblError.Text = "Username is illegal"; });
                        break;
                    case "1044":
                        Invoke((MethodInvoker)delegate { lblError.Text = "Other "; });
                        break;
                }
            }

        }

 

        private string padding(int num)
        {
            if (num > 9)
            {
                return num.ToString();
            }
            else
            {
                string ret = num.ToString();
                ret = ret.PadLeft(2, '0');
                return ret;
            }

        }

        private void SignUp_Load(object sender, EventArgs e)
        {
            Thread th = new Thread(() => handleThread(t));
            th.Start();
        }
    }
}
