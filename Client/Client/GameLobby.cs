﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;
namespace Client
{
    public partial class GameLobby : Form
    {
        private static string _players;
        private static int _numOfPlayers;
        private static int _timeForQuestion;
        private static string name;
        int numOfQuestionsGlobal;
        private static TcpClient tcpInt;
        private static string roomName;
        private static string roomId;
        public GameLobby(string players,int numOfQuestions,int timeForQuestion,string _name,TcpClient t,string _roomName,int _roomId)
        {
            InitializeComponent();
            numOfQuestionsGlobal = numOfQuestions;
            _players = players;
            _numOfPlayers = numOfQuestions;
            _timeForQuestion = timeForQuestion;
            name = _name;
            tcpInt = t;
            roomName = _roomName;
            
        }

        private void updateInfo()
        {
            Invoke((MethodInvoker)delegate
            {
                lblRoomName.Text = roomName;
                lblNumOfQuestions.Text = _numOfPlayers.ToString();
                lblTimePerQuestion.Text = _timeForQuestion.ToString();
            });
           
        }

    


        private void handleThread(TcpClient client)
        {
            Byte[] data;
            NetworkStream stream = tcpInt.GetStream();

            while (1 == 1)
            {

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length); //(**This receives the data using the byte method**)
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                string code = responseData.Substring(0, 3);
                switch (code)
                {
                    case "116":
                        Invoke((MethodInvoker)delegate
                        {
                            this.Hide();
                            Form wnd = new Signed(name, tcpInt);
                            wnd.ShowDialog();
                            this.Close();
                        });
                        break;
                    case "118":
                        Invoke((MethodInvoker)delegate
                        {
                            this.Hide();
                            Form wnd = new Game(responseData,name,roomName,numOfQuestionsGlobal, _timeForQuestion,tcpInt);
                            wnd.ShowDialog();
                            this.Close();
                        });
                        break;
                    case "106":
                        roomId = responseData.Substring(responseData.IndexOf(roomName) - 6, 4);
                        break;
                    case "108":
                        Invoke((MethodInvoker)delegate
                        {
                            lstbxPlayers.Items.Clear();
                            int playersNum = int.Parse(responseData.Substring(3, 1));
                            int j = 4;
                            for (int i = 0; i < playersNum; i++)
                            {
                                int length = int.Parse(responseData.Substring(j, 2));
                                j += 2;
                                string name = responseData.Substring(j, length);
                                j += length;
                                lstbxPlayers.Items.Add(name);
                            }

                        });
                        break;
                    default:
                        break;
                }
            }

        }

  
        private void GameLobby_Load(object sender, EventArgs e)
        {
            Thread t = new Thread(() => handleThread(tcpInt));
            t.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new Signed(name, tcpInt);
            wnd.ShowDialog();
            this.Close();   
        }
    }
}
