﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class AdminRoom : Form
    {
        string roomId;
        string user;
        TcpClient tcpInt;
        string roomNameGlobal;
        int questionsNo;
        int timeGlobal;
        DateTime delta;
        public AdminRoom(string userName,string maxPlayers,string time,string questions,string roomName,TcpClient client)
        {
            delta = System.DateTime.Now;
            roomNameGlobal = roomName;
            InitializeComponent();
            user = userName;
            lblUserName.Text = userName;
            lblTime.Text += time;
            lblRoomName.Text += roomName;
            lblNoQuestions.Text += questions;
            lblMaxPlayers.Text += maxPlayers;
            listBoxUsers.Items.Add(userName);
            tcpInt = client;
            questionsNo = int.Parse(questions);
            timeGlobal = int.Parse(time);

            Byte[] data = System.Text.Encoding.ASCII.GetBytes("205");
            NetworkStream stream = tcpInt.GetStream();
            stream.Write(data, 0, data.Length);
        }

        private void AdminRoom_Load(object sender, EventArgs e)
        {
            Thread t = new Thread(() => handleThread(tcpInt));
            t.Start();
        }



        private void handleThread(TcpClient client)
        {
            Byte[] data;
            NetworkStream stream = tcpInt.GetStream();

            while (1 == 1)
            {

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length); //(**This receives the data using the byte method**)
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                string code = responseData.Substring(0, 3);
                switch (code)
                {
                    case "116":
                        Invoke((MethodInvoker)delegate
                        {
                            this.Hide();
                            Form wnd = new Signed(user, tcpInt);    
                            wnd.ShowDialog();
                            this.Close();                       
                        });
                        break;
                    case "118":
                        Invoke((MethodInvoker)delegate
                        {
                            this.Hide();
                            Form wnd = new Game(responseData,user,roomNameGlobal,questionsNo,timeGlobal,tcpInt);
                            wnd.ShowDialog();
                            this.Close();
                        });
                        break;
                    case "106":
                        roomId = responseData.Substring(responseData.IndexOf(roomNameGlobal) - 6, 4);
                        break;
                    case "108":
                        Invoke((MethodInvoker)delegate 
                        {
                            listBoxUsers.Items.Clear();
                            int playersNum = int.Parse(responseData.Substring(3, 1));
                            int j = 4;
                            for (int i = 0; i < playersNum; i++)
                            {
                                int length = int.Parse(responseData.Substring(j, 2));
                                j += 2;
                                string name = responseData.Substring(j, length);
                                j += length;
                                listBoxUsers.Items.Add(name);
                            }
                        
                        });                      
                        break;
                    default:
                        break;
                }
            }

        }

        private void btnCloseRoom_Click(object sender, EventArgs e)
        {
            
            Byte[] data = System.Text.Encoding.ASCII.GetBytes("215");

       
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length); //(**This is to send data using the byte method**)
            
          
        }

        private void btnStartGame_Click(object sender, EventArgs e)
        {
            Byte[] data = System.Text.Encoding.ASCII.GetBytes("217");

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            // Send the message to the connected TcpServer. 
            stream.Write(data, 0, data.Length); //(**This is to send data using the byte method**)
        }
    }
}
