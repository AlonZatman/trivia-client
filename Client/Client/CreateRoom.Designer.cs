﻿namespace Client
{
    partial class CreateRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblRoomName = new System.Windows.Forms.Label();
            this.lblNumQuestions = new System.Windows.Forms.Label();
            this.lblPlayerNum = new System.Windows.Forms.Label();
            this.lblQuestionTime = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.txtRoomName = new System.Windows.Forms.TextBox();
            this.txtNumPlayers = new System.Windows.Forms.TextBox();
            this.txtQusetionNum = new System.Windows.Forms.TextBox();
            this.txtQuestionTime = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(314, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(250, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "Create A Room";
            // 
            // lblRoomName
            // 
            this.lblRoomName.AutoSize = true;
            this.lblRoomName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblRoomName.Location = new System.Drawing.Point(199, 118);
            this.lblRoomName.Name = "lblRoomName";
            this.lblRoomName.Size = new System.Drawing.Size(139, 25);
            this.lblRoomName.TabIndex = 1;
            this.lblRoomName.Text = "Room Name";
            // 
            // lblNumQuestions
            // 
            this.lblNumQuestions.AutoSize = true;
            this.lblNumQuestions.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblNumQuestions.Location = new System.Drawing.Point(199, 251);
            this.lblNumQuestions.Name = "lblNumQuestions";
            this.lblNumQuestions.Size = new System.Drawing.Size(237, 25);
            this.lblNumQuestions.TabIndex = 2;
            this.lblNumQuestions.Text = "Number Of Questions";
            // 
            // lblPlayerNum
            // 
            this.lblPlayerNum.AutoSize = true;
            this.lblPlayerNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblPlayerNum.Location = new System.Drawing.Point(199, 181);
            this.lblPlayerNum.Name = "lblPlayerNum";
            this.lblPlayerNum.Size = new System.Drawing.Size(210, 25);
            this.lblPlayerNum.TabIndex = 3;
            this.lblPlayerNum.Text = "Number Of Players";
            // 
            // lblQuestionTime
            // 
            this.lblQuestionTime.AutoSize = true;
            this.lblQuestionTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblQuestionTime.Location = new System.Drawing.Point(199, 319);
            this.lblQuestionTime.Name = "lblQuestionTime";
            this.lblQuestionTime.Size = new System.Drawing.Size(266, 25);
            this.lblQuestionTime.TabIndex = 4;
            this.lblQuestionTime.Text = "Time For Each Question";
            // 
            // btnCreate
            // 
            this.btnCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnCreate.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCreate.Location = new System.Drawing.Point(363, 386);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(147, 38);
            this.btnCreate.TabIndex = 5;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // txtRoomName
            // 
            this.txtRoomName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.txtRoomName.Location = new System.Drawing.Point(527, 118);
            this.txtRoomName.Name = "txtRoomName";
            this.txtRoomName.Size = new System.Drawing.Size(195, 20);
            this.txtRoomName.TabIndex = 6;
            this.txtRoomName.Text = "room_name";
            // 
            // txtNumPlayers
            // 
            this.txtNumPlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.txtNumPlayers.Location = new System.Drawing.Point(527, 181);
            this.txtNumPlayers.Name = "txtNumPlayers";
            this.txtNumPlayers.Size = new System.Drawing.Size(195, 20);
            this.txtNumPlayers.TabIndex = 7;
            this.txtNumPlayers.Text = "3";
            // 
            // txtQusetionNum
            // 
            this.txtQusetionNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.txtQusetionNum.Location = new System.Drawing.Point(527, 251);
            this.txtQusetionNum.Name = "txtQusetionNum";
            this.txtQusetionNum.Size = new System.Drawing.Size(195, 20);
            this.txtQusetionNum.TabIndex = 8;
            this.txtQusetionNum.Text = "5";
            // 
            // txtQuestionTime
            // 
            this.txtQuestionTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.txtQuestionTime.Location = new System.Drawing.Point(527, 324);
            this.txtQuestionTime.Name = "txtQuestionTime";
            this.txtQuestionTime.Size = new System.Drawing.Size(195, 20);
            this.txtQuestionTime.TabIndex = 9;
            this.txtQuestionTime.Text = "4";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(761, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 37);
            this.button1.TabIndex = 10;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(299, 354);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 24);
            this.lblError.TabIndex = 4;
            // 
            // CreateRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(872, 493);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtQuestionTime);
            this.Controls.Add(this.txtQusetionNum);
            this.Controls.Add(this.txtNumPlayers);
            this.Controls.Add(this.txtRoomName);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.lblQuestionTime);
            this.Controls.Add(this.lblPlayerNum);
            this.Controls.Add(this.lblNumQuestions);
            this.Controls.Add(this.lblRoomName);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Name = "CreateRoom";
            this.Text = "CreateRoom";
            this.Load += new System.EventHandler(this.CreateRoom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRoomName;
        private System.Windows.Forms.Label lblNumQuestions;
        private System.Windows.Forms.Label lblPlayerNum;
        private System.Windows.Forms.Label lblQuestionTime;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.TextBox txtRoomName;
        private System.Windows.Forms.TextBox txtNumPlayers;
        private System.Windows.Forms.TextBox txtQusetionNum;
        private System.Windows.Forms.TextBox txtQuestionTime;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblError;
    }
}