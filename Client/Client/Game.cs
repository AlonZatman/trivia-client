﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Game : Form
    {
        int timeGlobal;
        Button [] btnArr;
        private int timePassed;  
        int clickedOn = 5;
        TcpClient tcpInt;
        string userNameGlobal;
        DateTime delta;
        public Game(string firstQuestion118Code,string userName,string roomName,int questionNo ,int time,TcpClient tcp)
        {
            
            InitializeComponent();
            delta = System.DateTime.Now;
            timePassed = -5;
            userNameGlobal = userName;
            tcpInt = tcp;
            string[] arr = getQuestionAndAnswers(firstQuestion118Code);
            lblRoomName.Text = roomName;
            lblUserName.Text = userName;
            lblNoQuestion.Text = "\\" + questionNo.ToString();
            lblCurrQu.Text = "1";
            timer1.Interval = 1000;
            timer1.Tick += timer1_Tick;
            timer1.Start();
            lblQu.Text = arr[0];
            btnAnswer1.Text = arr[1];
            btnAnswer2.Text = arr[2];
            btnAnswer3.Text = arr[3];
            btnAnswer4.Text = arr[4];
            btnArr = new Button[4];
            btnArr[0] = btnAnswer1;
            btnArr[1] = btnAnswer2;
            btnArr[2] = btnAnswer3;
            btnArr[3] = btnAnswer4;
            timeGlobal = time;
        }

        private string[] getQuestionAndAnswers(string response)
        {
            string[] arr = new string[5];
            int j = 3;
            for(int i=0;i<5;i++)
            {
                string kipod = response.Substring(j, 3); 
                int length = int.Parse(response.Substring(j, 3));
                j += 3;
                string data = response.Substring(j, length);
                j += length;
                arr[i] = data;
            }

            return arr;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.label2.Text = System.DateTime.Now.Subtract(delta).ToString();
            this.delta = System.DateTime.Now;
            if (this.timePassed >= this.timeGlobal)
                TimeOut();
            else
                this.timePassed++;
            
        }

        private void TimeOut()
        {
            this.timePassed = -5;
            btnAnswer1.Enabled = false;
            btnAnswer2.Enabled = false;
            btnAnswer3.Enabled = false;
            btnAnswer4.Enabled = false;
            btnAnswer1.BackColor = Color.Red;
            btnAnswer2.BackColor = Color.Red;
            btnAnswer3.BackColor = Color.Red;
            btnAnswer4.BackColor = Color.Red;
            clickedOn = 5;

            string msg = "219510";
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length);

        }

        private void Game_Load(object sender, EventArgs e)
        {
            Thread t = new Thread(() => handleThread(tcpInt));
            t.Start();
        }

        private void handleThread(TcpClient client)
        {
            Byte[] data;
            NetworkStream stream = tcpInt.GetStream();

            while (1 == 1)
            {

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                string responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length); //(**This receives the data using the byte method**)
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                string code = responseData.Substring(0, 3);

                switch (code)
                {
                    case "120":
                        if(clickedOn!=5)
                        {

                            if (responseData[3] == '0')
                                btnArr[clickedOn - 1].BackColor = Color.Red;
                            else
                                btnArr[clickedOn - 1].BackColor = Color.Green;
                            //timer1.Enabled = false;
                        }
                        
                        break;
                    case "118":
                        
                        Invoke((MethodInvoker)delegate
                        {                         
                            Thread.Sleep(1000);
                            lblCurrQu.Text = (int.Parse(lblCurrQu.Text) + 1).ToString();
                            btnAnswer1.Enabled = true;
                            btnAnswer2.Enabled = true;
                            btnAnswer3.Enabled = true;
                            btnAnswer4.Enabled = true;                           
                            btnAnswer1.BackColor = Color.White;
                            btnAnswer2.BackColor = Color.White;
                            btnAnswer3.BackColor = Color.White;
                            btnAnswer4.BackColor = Color.White;
                            string[] arr = getQuestionAndAnswers(responseData);
                            btnAnswer1.Text = arr[1];
                            btnAnswer2.Text = arr[2];
                            btnAnswer3.Text = arr[3];
                            btnAnswer4.Text = arr[4];
                            lblQu.Text = arr[0];
                            this.timePassed = -5;
                        });
                        
                        break;
                    case "121":

                        int score = responseData.IndexOf(userNameGlobal);
                        int len = int.Parse(responseData.Substring(score - 2, 2));
                        string temp = responseData.Substring(score);
                        score = int.Parse(responseData.Substring(score+len, 2));
                        Invoke((MethodInvoker)delegate
                        {
                            MessageBox.Show("user name: " + userNameGlobal + " score: " +score);// responseData.Substring(responseData.Length - 2, 2));
                            this.Hide();
                            Form wnd = new Signed(userNameGlobal, tcpInt);
                            wnd.ShowDialog();
                            this.Close();   
                        });
                        break;
                }
            }

        }

        private void btnAnswer1_Click(object sender, EventArgs e)
        {
            btnAnswer1.Enabled = false;
            btnAnswer2.Enabled = false;
            btnAnswer3.Enabled = false;
            btnAnswer4.Enabled = false;
            Button btn = sender as Button;
            clickedOn = int.Parse(btn.Name.Substring(btn.Name.Length-1,1));         
            string msg = "219" + btn.Name[btn.Name.Length - 1] + this.timePassed.ToString(); 
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length);
        }

        private void btnAnswer2_Click(object sender, EventArgs e)
        {
            btnAnswer1_Click(sender, e);
        }

        private void btnAnswer3_Click(object sender, EventArgs e)
        {
            btnAnswer1_Click(sender, e);
        }

        private void btnAnswer4_Click(object sender, EventArgs e)
        {
            btnAnswer1_Click(sender, e);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Byte[] data = System.Text.Encoding.ASCII.GetBytes("222");

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length);
            this.Hide();
            Form wnd = new Signed(userNameGlobal, tcpInt);
            wnd.ShowDialog();
            this.Close();   
        }

        private void lblNoQuestion_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

    }
}
