﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
namespace Client
{
    public partial class JoinRoom : Form
    {
        string name;
        TcpClient tcpInt;
        private static string roomsAvailable;
        public string _players;
        private  Dictionary<string, int> roomList;
        private int numOfQuestions;
        private int timeForQuestion;
        private string _roomName;
        private int _roomId;
        private string[] _rooms;
        private int[] roomIds;
        private bool isJoined;

        public JoinRoom(string userName, TcpClient t)
        {
            InitializeComponent();
            name = userName;
            tcpInt = t;
            isJoined = false;
            _roomName = "";
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new Signed(name,tcpInt);
            wnd.ShowDialog();
            this.Close();     
        }

        private void handleThread(TcpClient client)
        {
            Byte[] data;
            NetworkStream stream = tcpInt.GetStream();

            while (1 == 1)
            {

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length); //(**This receives the data using the byte method**)
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

                if(responseData.Substring(0,3)=="106")
                {
                    roomsAvailable = responseData;
                }
                else if (responseData.Substring(0, 3) == "108")
                {
                    _players = responseData;
                }
                else if (responseData.Substring(0, 3) == "110")
                {
                    if(responseData.Substring(3,1)=="0")
                    {
                        numOfQuestions = int.Parse(responseData.Substring(4, 2));
                        timeForQuestion = int.Parse(responseData.Substring(6));
                        isJoined = true;
                        startGameLobby();
                    }
                    else if(responseData.Substring(3,1)=="1")
                    {
                        MessageBox.Show("Error,room is full", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (responseData.Substring(3, 1) == "2")
                    {
                        MessageBox.Show("Error,room does not exist/other reason", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

        }
        
        private void startGameLobby()
        {
            Invoke((MethodInvoker)delegate {
                this.Hide();
                Form wnd = new GameLobby(_players, numOfQuestions, timeForQuestion, name, tcpInt, _roomName, _roomId);
                wnd.ShowDialog();
                this.Close();
            });
            
        }

        private void JoinRoom_Load(object sender, EventArgs e)
        {
           
            Thread th = new Thread(() => handleThread(tcpInt));
            th.Start();
            getRooms();
        }

        private void getRooms()
        {
            int numOfRooms = 0;

            string rooms = "";
            var roomList = new Dictionary<string, int>();

            int roomId = 0;
            int roomNameSize = 0;
            string roomName = "";

            string msg = "205";

            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length);
            Thread.Sleep(200);
            rooms = roomsAvailable;
            if (int.Parse(rooms.Substring(3, 4)) == 0)
            {
                MessageBox.Show("Error joining room", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                numOfRooms = int.Parse(roomsAvailable.Substring(3, 4));
                
                roomIds = new int[numOfRooms];
                _rooms = new string[numOfRooms];
                rooms = roomsAvailable.Substring(7);
                for (int i = 0; i < numOfRooms; i++)
                {
                    roomId = int.Parse(rooms.Substring(0, 4));
                    roomNameSize = int.Parse(rooms.Substring(4, 2));
                    roomName = rooms.Substring(6, roomNameSize);
                    _rooms[i] = roomName;
                    roomIds[i] = roomId;
                 //   roomList[roomName]=roomId;
                    lstbxRooms.Items.Add(roomName);
                }
            }
        }

        private void btnJoin_Click(object sender, EventArgs e)
        {
            string roomName = lstbxRooms.SelectedItem.ToString();

            string msg = "209";
            int index = Array.IndexOf(_rooms, roomName);
            msg += roomIds[index].ToString();
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length);
            _roomName = roomName;
            
            _roomId = roomIds[index];
        }

        private void lstbxRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            string roomName ="";
            try
            {
                roomName = lstbxRooms.SelectedItem.ToString();
            }
            catch
            {
            }

           
            string tplayers= "";
            int userLen = 0;
            string msg = "207";
            int index = Array.IndexOf(_rooms, roomName);
            try
            {
                msg += roomIds[index].ToString(); 
            }
            catch
            {
            }
              
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length);
            Thread.Sleep(150);
            tplayers = _players;
            int userNum = 0;
            try
            {
                userNum=int.Parse(tplayers.Substring(3, 1));
            }
            catch { }
            try
            {
                tplayers = tplayers.Substring(4);
            }
            catch { }
            
            for(int i = 0; i < userNum ;i++)
            {
                userLen = int.Parse(tplayers.Substring(0, 2));
                lstbxPlayers.Items.Add(tplayers.Substring(2, userLen));
                tplayers = tplayers.Substring(2 + userLen);
            }
            Invoke((MethodInvoker)delegate
            {
                btnJoin.Enabled = true;
                lstbxPlayers.Visible = true;
                label2.Visible = true;
            });
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            lstbxRooms.Items.Clear();
            getRooms();
        }
    }
}
