﻿namespace Client
{
    partial class AdminRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxUsers = new System.Windows.Forms.ListBox();
            this.lblRoomName = new System.Windows.Forms.Label();
            this.lblMaxPlayers = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblNoQuestions = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.btnCloseRoom = new System.Windows.Forms.Button();
            this.btnStartGame = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxUsers
            // 
            this.listBoxUsers.FormattingEnabled = true;
            this.listBoxUsers.Location = new System.Drawing.Point(298, 137);
            this.listBoxUsers.Name = "listBoxUsers";
            this.listBoxUsers.Size = new System.Drawing.Size(207, 134);
            this.listBoxUsers.TabIndex = 0;
            // 
            // lblRoomName
            // 
            this.lblRoomName.AutoSize = true;
            this.lblRoomName.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblRoomName.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblRoomName.Location = new System.Drawing.Point(202, 22);
            this.lblRoomName.Name = "lblRoomName";
            this.lblRoomName.Size = new System.Drawing.Size(309, 29);
            this.lblRoomName.TabIndex = 2;
            this.lblRoomName.Text = "You are connected to room ";
            // 
            // lblMaxPlayers
            // 
            this.lblMaxPlayers.AutoSize = true;
            this.lblMaxPlayers.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblMaxPlayers.Location = new System.Drawing.Point(154, 90);
            this.lblMaxPlayers.Name = "lblMaxPlayers";
            this.lblMaxPlayers.Size = new System.Drawing.Size(70, 13);
            this.lblMaxPlayers.TabIndex = 4;
            this.lblMaxPlayers.Text = "Max Players: ";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblUserName.Location = new System.Drawing.Point(29, 22);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 13);
            this.lblUserName.TabIndex = 1;
            // 
            // lblNoQuestions
            // 
            this.lblNoQuestions.AutoSize = true;
            this.lblNoQuestions.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblNoQuestions.Location = new System.Drawing.Point(356, 90);
            this.lblNoQuestions.Name = "lblNoQuestions";
            this.lblNoQuestions.Size = new System.Drawing.Size(110, 13);
            this.lblNoQuestions.TabIndex = 5;
            this.lblNoQuestions.Text = "Number of questions: ";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblTime.Location = new System.Drawing.Point(584, 90);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(91, 13);
            this.lblTime.TabIndex = 6;
            this.lblTime.Text = "Time for question:";
            // 
            // btnCloseRoom
            // 
            this.btnCloseRoom.Location = new System.Drawing.Point(311, 322);
            this.btnCloseRoom.Name = "btnCloseRoom";
            this.btnCloseRoom.Size = new System.Drawing.Size(177, 31);
            this.btnCloseRoom.TabIndex = 7;
            this.btnCloseRoom.Text = "Close Room";
            this.btnCloseRoom.UseVisualStyleBackColor = true;
            this.btnCloseRoom.Click += new System.EventHandler(this.btnCloseRoom_Click);
            // 
            // btnStartGame
            // 
            this.btnStartGame.Location = new System.Drawing.Point(311, 383);
            this.btnStartGame.Name = "btnStartGame";
            this.btnStartGame.Size = new System.Drawing.Size(177, 31);
            this.btnStartGame.TabIndex = 8;
            this.btnStartGame.Text = "Start game";
            this.btnStartGame.UseVisualStyleBackColor = true;
            this.btnStartGame.Click += new System.EventHandler(this.btnStartGame_Click);
            // 
            // AdminRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(994, 510);
            this.Controls.Add(this.btnStartGame);
            this.Controls.Add(this.btnCloseRoom);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblNoQuestions);
            this.Controls.Add(this.lblMaxPlayers);
            this.Controls.Add(this.lblRoomName);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.listBoxUsers);
            this.Name = "AdminRoom";
            this.Text = "Room";
            this.Load += new System.EventHandler(this.AdminRoom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxUsers;
        private System.Windows.Forms.Label lblRoomName;
        private System.Windows.Forms.Label lblMaxPlayers;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblNoQuestions;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Button btnCloseRoom;
        private System.Windows.Forms.Button btnStartGame;
    }
}