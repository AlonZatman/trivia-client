﻿namespace Client
{
    partial class GameLobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblConncted = new System.Windows.Forms.Label();
            this.lblRoomName = new System.Windows.Forms.Label();
            this.lblPlayersConnected = new System.Windows.Forms.Label();
            this.lstbxPlayers = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblNoChange1 = new System.Windows.Forms.Label();
            this.lblNumOfQuestions = new System.Windows.Forms.Label();
            this.lblNoChange2 = new System.Windows.Forms.Label();
            this.lblTimePerQuestion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblConncted
            // 
            this.lblConncted.AutoSize = true;
            this.lblConncted.Location = new System.Drawing.Point(168, 49);
            this.lblConncted.Name = "lblConncted";
            this.lblConncted.Size = new System.Drawing.Size(297, 25);
            this.lblConncted.TabIndex = 0;
            this.lblConncted.Text = "You are connected to room";
            // 
            // lblRoomName
            // 
            this.lblRoomName.AutoSize = true;
            this.lblRoomName.Location = new System.Drawing.Point(484, 49);
            this.lblRoomName.Name = "lblRoomName";
            this.lblRoomName.Size = new System.Drawing.Size(134, 25);
            this.lblRoomName.TabIndex = 1;
            this.lblRoomName.Text = "room_name";
            // 
            // lblPlayersConnected
            // 
            this.lblPlayersConnected.AutoSize = true;
            this.lblPlayersConnected.Location = new System.Drawing.Point(195, 148);
            this.lblPlayersConnected.Name = "lblPlayersConnected";
            this.lblPlayersConnected.Size = new System.Drawing.Size(214, 25);
            this.lblPlayersConnected.TabIndex = 2;
            this.lblPlayersConnected.Text = "Players connected:";
            // 
            // lstbxPlayers
            // 
            this.lstbxPlayers.FormattingEnabled = true;
            this.lstbxPlayers.ItemHeight = 25;
            this.lstbxPlayers.Items.AddRange(new object[] {
            ""});
            this.lstbxPlayers.Location = new System.Drawing.Point(195, 189);
            this.lstbxPlayers.Name = "lstbxPlayers";
            this.lstbxPlayers.Size = new System.Drawing.Size(379, 129);
            this.lstbxPlayers.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(271, 367);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 38);
            this.button1.TabIndex = 6;
            this.button1.Text = "Leave Room";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblNoChange1
            // 
            this.lblNoChange1.AutoSize = true;
            this.lblNoChange1.Location = new System.Drawing.Point(83, 76);
            this.lblNoChange1.Name = "lblNoChange1";
            this.lblNoChange1.Size = new System.Drawing.Size(236, 25);
            this.lblNoChange1.TabIndex = 7;
            this.lblNoChange1.Text = "Number of questions:";
            // 
            // lblNumOfQuestions
            // 
            this.lblNumOfQuestions.AutoSize = true;
            this.lblNumOfQuestions.Location = new System.Drawing.Point(325, 76);
            this.lblNumOfQuestions.Name = "lblNumOfQuestions";
            this.lblNumOfQuestions.Size = new System.Drawing.Size(25, 25);
            this.lblNumOfQuestions.TabIndex = 8;
            this.lblNumOfQuestions.Text = "0";
            // 
            // lblNoChange2
            // 
            this.lblNoChange2.AutoSize = true;
            this.lblNoChange2.Location = new System.Drawing.Point(388, 74);
            this.lblNoChange2.Name = "lblNoChange2";
            this.lblNoChange2.Size = new System.Drawing.Size(220, 25);
            this.lblNoChange2.TabIndex = 9;
            this.lblNoChange2.Text = "Time per questions:";
            // 
            // lblTimePerQuestion
            // 
            this.lblTimePerQuestion.AutoSize = true;
            this.lblTimePerQuestion.Location = new System.Drawing.Point(614, 76);
            this.lblTimePerQuestion.Name = "lblTimePerQuestion";
            this.lblTimePerQuestion.Size = new System.Drawing.Size(25, 25);
            this.lblTimePerQuestion.TabIndex = 10;
            this.lblTimePerQuestion.Text = "0";
            // 
            // GameLobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.lblTimePerQuestion);
            this.Controls.Add(this.lblNoChange2);
            this.Controls.Add(this.lblNumOfQuestions);
            this.Controls.Add(this.lblNoChange1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lstbxPlayers);
            this.Controls.Add(this.lblPlayersConnected);
            this.Controls.Add(this.lblRoomName);
            this.Controls.Add(this.lblConncted);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "GameLobby";
            this.Text = "Game Lobby";
            this.Load += new System.EventHandler(this.GameLobby_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblConncted;
        private System.Windows.Forms.Label lblRoomName;
        private System.Windows.Forms.Label lblPlayersConnected;
        private System.Windows.Forms.ListBox lstbxPlayers;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblNoChange1;
        private System.Windows.Forms.Label lblNumOfQuestions;
        private System.Windows.Forms.Label lblNoChange2;
        private System.Windows.Forms.Label lblTimePerQuestion;
    }
}