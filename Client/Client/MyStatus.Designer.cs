﻿namespace Client
{
    partial class MyStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMyPerformance = new System.Windows.Forms.Label();
            this.lblNumOfGames = new System.Windows.Forms.Label();
            this.lblRight = new System.Windows.Forms.Label();
            this.lblWrong = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMyPerformance
            // 
            this.lblMyPerformance.AutoSize = true;
            this.lblMyPerformance.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblMyPerformance.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblMyPerformance.Location = new System.Drawing.Point(176, 63);
            this.lblMyPerformance.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblMyPerformance.Name = "lblMyPerformance";
            this.lblMyPerformance.Size = new System.Drawing.Size(327, 42);
            this.lblMyPerformance.TabIndex = 0;
            this.lblMyPerformance.Text = "My Performance: ";
            // 
            // lblNumOfGames
            // 
            this.lblNumOfGames.AutoSize = true;
            this.lblNumOfGames.Location = new System.Drawing.Point(173, 145);
            this.lblNumOfGames.Name = "lblNumOfGames";
            this.lblNumOfGames.Size = new System.Drawing.Size(218, 25);
            this.lblNumOfGames.TabIndex = 1;
            this.lblNumOfGames.Text = "Number of games - ";
            // 
            // lblRight
            // 
            this.lblRight.AutoSize = true;
            this.lblRight.Location = new System.Drawing.Point(173, 193);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(290, 25);
            this.lblRight.TabIndex = 2;
            this.lblRight.Text = "Number of right answers - ";
            // 
            // lblWrong
            // 
            this.lblWrong.AutoSize = true;
            this.lblWrong.Location = new System.Drawing.Point(173, 244);
            this.lblWrong.Name = "lblWrong";
            this.lblWrong.Size = new System.Drawing.Size(306, 25);
            this.lblWrong.TabIndex = 3;
            this.lblWrong.Text = "Number of wrong answers - ";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(173, 295);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(281, 25);
            this.lblTime.TabIndex = 4;
            this.lblTime.Text = "Average time to answer - ";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(528, 442);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 39);
            this.button1.TabIndex = 5;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MyStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(711, 741);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblWrong);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.lblNumOfGames);
            this.Controls.Add(this.lblMyPerformance);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "MyStatus";
            this.Text = "MyStatus";
            this.Load += new System.EventHandler(this.MyStatus_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMyPerformance;
        private System.Windows.Forms.Label lblNumOfGames;
        private System.Windows.Forms.Label lblRight;
        private System.Windows.Forms.Label lblWrong;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Button button1;
    }
}