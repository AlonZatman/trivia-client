﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Signed : Form
    {
        TcpClient tcpInt;
        string name;
        public Signed(string userName, TcpClient t)
        {
            InitializeComponent();
            lblHelloUser.Text = "Hello " + userName;
            tcpInt = t;
            name = userName;
        }

        private void handleThread(TcpClient client)
        {
            Byte[] data;
            NetworkStream stream = tcpInt.GetStream();

            while (1 == 1)
            {

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length); //(**This receives the data using the byte method**)
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

                switch (responseData)
                {
                   
                }
            }

        }

        private void btnSignOut_Click(object sender, EventArgs e)
        {
            string send = "201";
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(send);

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            // Send the message to the connected TcpServer. 
            stream.Write(data, 0, data.Length); //(**This is to send data using the byte method**)
            this.Hide();
            Form wnd = new Form1();
            wnd.ShowDialog();
            this.Close();          
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            string send = "201";
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(send);

            // Get a client stream for reading and writing. 
            NetworkStream stream = tcpInt.GetStream();

            stream.Write(data, 0, data.Length);

            Application.Exit();
        }

        private void btnJoinRoom_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new JoinRoom(name,tcpInt);
            wnd.ShowDialog();
        }

        private void btnCreateRoom_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new CreateRoom(name,tcpInt);
            wnd.ShowDialog();
        }

        private void btnMyStatus_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new MyStatus(name, tcpInt);
            wnd.ShowDialog();
        }

        private void btnBestScores_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form wnd = new BestScores(name, tcpInt);
            wnd.ShowDialog();
        }
    }
}
